import React, {useEffect} from "react";
import M from "materialize-css/dist/js/materialize.min.js";
// Components
import Login from "./components/auth/Login";
import SideNav from "./components/layouts/SideNav";
import Dashboard from "./components/auth/Dashboard";
// Router
import {BrowserRouter as Router, Route, Switch} from "react-router-dom";
import PrivateRoute from "./components/PrivateRoute";

// Redux
import {Provider} from "react-redux";
import Store from "./Store";
// Styles
import "./materialize.css";
import "./App.css";

// Actions
import {loadUser} from "./actions/AuthActions";

function App() {
  useEffect(() => {
    M.AutoInit();
    Store.dispatch(loadUser());
    // estlint-disable-next-line
  }, []);

  return (
    <Provider store={Store}>
      <Router>
        <SideNav />
        {/* <Alert /> */}
        <div className='App'>
          <Switch>
            <Route exact path='/' component={Login} />
          </Switch>
          <div className='padding-body'>
            <Switch>
              <PrivateRoute exact path='/dashboard' component={Dashboard} />
            </Switch>
          </div>
        </div>
      </Router>
    </Provider>
  );
}

export default App;
