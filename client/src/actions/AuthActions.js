import {
  USER_LOADED,
  AUTH_ERROR,
  LOGIN,
  LOGIN_ERROR,
  LOGOUT,
  CLEAR_PROFILE,
  CLEAR_TOKEN
} from "./Types";
import axios from "axios";
import {setAlert} from '../actions/AlertAction'
import setAuthToken from "../util/setAuthToken";

export const loadUser = () => async dispatch => {
  const token = localStorage.getItem("panelToken");
  if (localStorage.panelToken) {
    setAuthToken(token);
  } else {
    dispatch({
      type: AUTH_ERROR
    });
    return false;
  }
  try {
    const res = await axios.get("/api/users/get-user-details");

    dispatch({
      type: USER_LOADED,
      payload: res.data
    });
  } catch (error) {
    const errors = error.response.data.errors;

    if (errors) {
      errors.forEach(err => dispatch(setAlert(err.msg, "danger")));
    }
    dispatch({
      type: AUTH_ERROR
    });
  }
};

//Login
export const login = ({userName, password}) => async dispatch => {
  const config = {
    headers: {
      "Content-Type": "application/json"
    }
  };

  const body = JSON.stringify({userName, password});
  try {
    const res = await axios.post("/api/auth/login", body, config);

    dispatch({
      type: LOGIN,
      payload: res.data
    });
    // dispatch(loadUser());
  } catch (error) {
    const errors = error.response.data.errors;

    dispatch({
      type: LOGIN_ERROR
    });
    if (errors) {
      errors.forEach(err => dispatch(setAlert(err.msg, "danger")));
    }
  }
};

export const logout = () => dispatch => {
  dispatch({
    type: LOGOUT
  });
  dispatch({
    type: CLEAR_PROFILE
  });
  dispatch({
    type: CLEAR_TOKEN
  });
};
