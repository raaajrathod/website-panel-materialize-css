import React from "react";
import PropTypes from "prop-types";
import {connect} from "react-redux";
import M from "materialize-css/dist/js/materialize.min.js";
import "../../materialize.css";

const Alert = ({alerts}) =>
  alerts !== null &&
  alerts.length > 0 &&
  alerts.map(alert => M.toast({html: alert.msg}));

Alert.propTypes = {
  alerts: PropTypes.array.isRequired
};

const mapStateToProps = state => ({
  alerts: state.AlertReducer
});
export default connect(
  mapStateToProps,
  {}
)(Alert);
