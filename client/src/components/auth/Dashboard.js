import React, {useEffect} from "react";
import {loadUser} from "../../actions/AuthActions";
import {connect} from "react-redux";

const Dashboard = ({loadUser}) => {
  useEffect(() => {
    loadUser();
  });
  return (
    <div>
      <p>Hello World</p>
    </div>
  );
};

export default connect(
  null,
  {loadUser}
)(Dashboard);
