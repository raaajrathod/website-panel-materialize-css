import React, {Fragment, useState} from "react";
import M from "materialize-css/dist/js/materialize.min.js";
import {connect} from "react-redux";
import {login} from "../../actions/AuthActions";
import {Redirect} from "react-router-dom";

const Login = ({Alert, login, Auth :{ isAuthenticated}}) => {
  const [formData, setFormData] = useState({
    userName: "",
    password: ""
  });

  const onChange = e => {
    setFormData({...formData, [e.target.name]: e.target.value});
  };
  const submitForm = e => {
    e.preventDefault();
    let isValid = true;

    if (userName === "") {
      return M.toast({html: "User Name is Required", classes: "errorToast"});
    }

    if (password === "") {
      return M.toast({html: "Password is Required", classes: "errorToast"});
    }

    if (isValid) {
      // Submit Form
      login(formData);
    }

    Alert.map(alert => {
      M.toast({html: alert.msg, classes: "errorToast"});
    });
  };

  // Redirect to Dashboard
  if (isAuthenticated) {
    return <Redirect to='/dashboard' />;
  }

  const {userName, password} = formData;
  return (
    <Fragment>
      <div className='row'>
        <div className='col s12' style={{paddingTop: "5%"}}>
          <div className='row noBottomMargin'>
            <div className='col s12 l3 offset-l5'>
              <h4 className='grey-text' style={{paddingLeft: "25%"}}>
                Sign In
              </h4>
            </div>
          </div>
          <div className='row noBottomMargin'>
            <div className='col s12 l3 input-field offset-l5'>
              <input
                type='text'
                id='userName'
                className='validate col l11'
                name='userName'
                value={userName}
                onChange={e => onChange(e)}
              />
              <label htmlFor='userName'>User Name</label>
            </div>
          </div>
          <div className='row'>
            <div className='col s12 l3 input-field offset-l5'>
              <input
                type='password'
                id='password'
                name='password'
                className='validate col l11'
                value={password}
                onChange={e => onChange(e)}
              />
              <label htmlFor='password'>Password</label>
            </div>
          </div>
          <div className='row noBottomMargin'>
            <div className='col s12 l3 offset-l5'>
              <button
                className='btn col s11 waves-effect waves-light'
                onClick={e => submitForm(e)}>
                Sign In
              </button>
            </div>
          </div>
        </div>
      </div>
    </Fragment>
  );
};

const mapStateToProps = state => ({
  Alert: state.AlertReducer,
  Auth: state.AuthReducer
});

export default connect(
  mapStateToProps,
  {login}
)(Login);
